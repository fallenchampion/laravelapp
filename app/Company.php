<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Company extends Model
{
    protected $table = "company";
    public static function getAllNameCompany()
    {
        $name_company = Company::select('name')->get();
        return $name_company;
    }
}
