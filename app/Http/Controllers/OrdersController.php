<?php

namespace App\Http\Controllers;

use App\Company;
use App\Orders;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;

class OrdersController extends Controller
{
    public function create()
    {
        $name_company = Company::getAllNameCompany();
        if (!empty($name_company))
        {

            foreach ($name_company as   $key => $name_compan)
            {
                $key++;
                $items_name[ $key] = $name_compan->name;


            }

        }
        return view('orders.create')->with('items_name',$items_name);

    }
    public function store(Request $request)
    {

        if ($request->isMethod('post')) {

            $order = new Orders();
            $data = $this->validate($request, [
                'company' => 'required|max:150',
                'place' => 'required | max:150',
                'address' => 'required|max:250',
                'descriptions' => 'max:10000',
                'user_id' => 'integer',

            ]);

            //create new object for model Orders
            $status = $order->saveOrder($data);

            //if model can saving - we save
            if ($status == 1) {
                return redirect('/orders/create')->with('succsess', 'Article created!');
            } else {
                return redirect('/orders/create')->with('error', 'Woops article not created!');
            }
        }
        else{
            return redirect('/orders/create')->with('error', 'Woops article not created!');
        }




    }
    public function index()
    {
        $orders = Orders::getOrdersUser();
        $count_orders = count($orders);

        return view('orders.index')->with('orders', $orders)->with('count_orders',$count_orders);

    }

    public function view($id)
    {
        $order = Orders::getViewOrder($id);

        return view('orders.view')->with('article', $order);

    }
    public function edit($id)
    {
        $order = Orders::getOrderForEdit($id);
        $name_company = Company::getAllNameCompany();
        if (!empty($name_company))
        {
            foreach ($name_company as   $key => $name_compan)
            {
                $key++;
                $items_name[ $key] = $name_compan->name;


            }
        }

        return view('orders.edit')->with('order', $order)->with('items_name', $items_name);

    }
    public function update($id, Request $request)
    {
        $order = new Orders();
        $data = $this->validate($request, [
            'company' => 'required|max:150',
            'place' => 'required | max:150',
            'address' => 'required|max:250',
            'descriptions' => 'max:10000',
            'user_id' => 'integer',
        ]);
        $data['id'] = $id;


        $status = $order->updateOrder($data);

        //if model can saving - we save
        if ($status == 1) {
            return redirect('/orders')->with('succsess', 'Article updated!');
        } else {
            return Redirect::back()->with('error', 'Woops article not aupdated!');
        }


    }

}
