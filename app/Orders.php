<?php

namespace App;

use App\Status;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class Orders extends Model
{
    protected $table = "orders";

  public static function getOrdersUser() // get orders for currently user
  {
      $userId = Auth::id();
      $orderss = Orders::where('user_id',$userId)->paginate(5);
      return $orderss;
  }
  public static function getViewOrder($id)
  {
      $order = Orders::where('id',$id)->first();
      return $order;

  }

  public static function getOrderForEdit($id)
  {
      $orders = Orders::where('id',$id)->first();
      return $orders;

  }

  public function saveOrder($data)
  {

      $userId = Auth::id();

      // get data from form controller

      $this->company = $data['company'];
      $this->place = $data['place'];
      $this->address = $data['address'];
      $this->descriptions = $data['descriptions'];
      $this->user_id = $userId;
      $this->status = 1;

      // trying save data of order

      if ($this->save()){
          return 1;
      }
      else{
          return 0;
      }
  }
  public function updateOrder($data)
  {
      $userId = Auth::id();
      $this->company = $data['company'];
      $this->place = $data['place'];
      $this->address = $data['address'];
      $this->descriptions = $data['descriptions'];
      $this->user_id = $userId;
      $this->status = 1;

      $orders = $this->find($data['id']);
      $orders->company = $data['company'];
      $orders->place = $data['place'];
      $orders->address = $data['address'];
      $orders->descriptions = $data['descriptions'];
      $orders->user_id = $userId;
      $orders->status = 1;

      if ($orders->save()){
          return 1;
      }
      else{
          return 0;
      }
  }

  /*public function Statusorder()
  {
        return $this->belongsTo('App\Status');
  }
    */
    public function companys()
    {
        return $this->hasOne(Company::class, 'id', 'company');
    }

    public function statuses()
    {
        return $this->hasOne(Status::class, 'id', 'status');
    }

}
