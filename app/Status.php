<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Status extends Model
{
    protected $table = "status";

    public static function getStatusOrder()
    {
        $status = Status::select('name')->get();
        return $status;
    }
}
