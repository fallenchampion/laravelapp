@extends('layouts.manager')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">Dashboard</div>

                    <div class="card-body">
                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif



                        @section('content')
                            <h1>Home</h1>
                            <div class="text-main-page">
                                Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aperiam aut consequatur culpa dolor earum excepturi facilis itaque laudantium libero odio, omnis placeat, provident quas quisquam rem sed sint ullam voluptatum.
                            </div>
                        @endsection

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
