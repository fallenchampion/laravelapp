@extends('layouts.app')

@section('content')
<h1>Create articles</h1>


@include('inc.message')


<script>
   /* $(function() {
        $('#name-order').on('input', function () {
            $('#url-order').val($(this).val().toLowerCase().replace(/ /g, '-'));
        });
    });*/
</script>

<div class="block-create-order text-left">


    {{ Form::open(array('url' => '/orders/create')) }}
        <div class="form-group">
            {!! Form::label('company-order', 'Company:', ['class' => 'control-label']) !!}

            {{  Form::select('company', $items_name, null, ['class' => 'form-control','id'=>'company-order'])}}

        </div>
        <div class="form-group">
            {!! Form::label('place-order', 'Place:', ['class' => 'control-label']) !!}
            {!! Form::Text('place', null, ['class' => 'form-control','placeholder' =>'Place','id'=>'place-order']) !!}
        </div>
    <div class="form-group">
        {!! Form::label('address-order', 'Address:', ['class' => 'control-label']) !!}
        {!! Form::Text('address', null, ['class' => 'form-control','placeholder' =>'Address','id'=>'address-order']) !!}
    </div>

        <div class="form-group">
            {!! Form::label('descriptions-order', 'Descriptions:', ['class' => 'control-label']) !!}
            {!! Form::Textarea('descriptions', null, ['class' => 'form-control','placeholder' =>'Descriptions order','id'=>'descriptions-order']) !!}
        </div>



        <div>
            {!! Form::submit('Send', ['class' => 'btn btn-primary']) !!}

        </div>
    {{ Form::close() }}

</div>
@endsection
