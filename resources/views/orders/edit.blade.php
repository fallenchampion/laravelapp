@extends('layouts.app')

@section('content')
    <h1>Update orders</h1>

    @include('inc.message')

    <div class="block-update-order text-left">

            {{ Form::open(array('url' => 'orders/update/'.$order->id)) }}

        <div class="form-group">
            {!! Form::label('company-order', 'Company:', ['class' => 'control-label']) !!}

            {{  Form::select('company', $items_name, $order->company, ['class' => 'form-control','id'=>'company-order'])}}

        </div>
        <div class="form-group">
            {!! Form::label('place-order', 'Place:', ['class' => 'control-label']) !!}
            {!! Form::Text('place',  $order->place, ['class' => 'form-control','placeholder' =>'Place','id'=>'place-order']) !!}
        </div>
        <div class="form-group">
            {!! Form::label('address-order', 'Address:', ['class' => 'control-label']) !!}
            {!! Form::Text('address', $order->address, ['class' => 'form-control','placeholder' =>'Address','id'=>'address-order']) !!}
        </div>

        <div class="form-group">
            {!! Form::label('descriptions-order', 'Descriptions:', ['class' => 'control-label']) !!}
            {!! Form::Textarea('descriptions', $order->descriptions, ['class' => 'form-control','placeholder' =>'Descriptions order','id'=>'descriptions-order']) !!}
        </div>




        {!! Form::submit('Update Entry', ['class' => 'btn btn-primary']) !!}

            {!! Form::close() !!}

        </div>
@endsection