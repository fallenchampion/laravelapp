@extends('layouts.app')

@section('content')
    <h1>orders</h1>
    <div class="block-create-order text-right">
        <a href="{{ url('orders/create') }}">
            <button class="btn btn-default">
                Add orders

            </button>

        </a>

    </div>
    @include('inc.message')
        @if(!empty($orders->items()))
            <ul class="list-group ">
                <p class="text-left"> Count my orders: {{$count_orders}}</p>
                @foreach($orders as $order)

                        <li class="list-group-item my-list-group text-left">
                           <h1>

                                   {{$order->companys->name}}
                              <!-- <span>Date created: {{$order->created_at}}</span>-->
                              <span>Status: {{$order->statuses->name}}</span>
                             <a href="{{ url('/orders/edit/'.$order->id) }}">
                                 <i class="fa fa-pencil" aria-hidden="true"></i>
                             </a>

                           </h1>
                            <div>
                                {{$order->descriptions}}
                            </div>

                        </li>

                @endforeach
            </ul>

        @else
        <h1 class="text-left">Not have order</h1>

        @endif
@endsection