@extends('layouts.app')

@section('content')
    <h1>Article named {{$article->name}}</h1>

    <div class="full-descriptions">
        {{$article->full_descriptions}}
    </div>

@endsection
