<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@index');

Route::get('about', 'PagesController@about')->middleware('auth');
//routing for customer pages
Route::get('orders', 'OrdersController@index')->middleware('auth');

Route::get('orders/create', 'OrdersController@create')->middleware('auth');
Route::post('orders/create', 'OrdersController@store')->middleware('auth');
Route::get('orders/view/{url}', 'OrdersController@view')->middleware('auth');
Route::get('orders/edit/{id}', 'OrdersController@edit')->middleware('auth');
Route::post('orders/update/{id}', 'OrdersController@update')->middleware('auth');
Route::get('company', 'CompanyController@index')->middleware('auth');
Auth::routes();

Route::get('/home', 'HomeController@index')->name('home')->middleware('auth');
